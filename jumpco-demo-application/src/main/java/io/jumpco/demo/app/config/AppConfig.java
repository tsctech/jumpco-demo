package io.jumpco.demo.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.WebSphereUowTransactionManager;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true, mode = AdviceMode.PROXY)
@Import({io.jumpco.demo.todo.model.config.ModuleConfig.class, RepositoryRestMvcConfiguration.class})
@ComponentScan(basePackages = {"io.jumpco.demo.app.utils", "io.jumpco.demo.app.rest"})
@PropertySource("classpath:/jumpco-demo.properties")
public class AppConfig {
    @Value("${transaction.platform:JDBC}")
    private String transactionPlatform;

    @Bean(name = "validator")
    public LocalValidatorFactoryBean validatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        return configurer;
    }

    @Bean(name = "serviceMessageSource")
    public MessageSource messageSource() {
        ResourceBundleMessageSource bean = new ResourceBundleMessageSource();
        bean.setBasename("demo-messages");
        return bean;
    }

    @Bean(name = "messageAccessor")
    @Autowired
    @Qualifier("serviceMessageSource")
    public MessageSourceAccessor messageSourceAccessor(MessageSource messageSource) {
        return new MessageSourceAccessor(messageSource);
    }

    @Bean(name = "transactionManager")
    @Autowired
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
        if (transactionPlatform.equalsIgnoreCase("JTA")) {
            return new WebSphereUowTransactionManager();
        } else if (transactionPlatform.equalsIgnoreCase("JDBC")) {
            return new JpaTransactionManager(entityManagerFactory);
        } else {
            final String msg = String.format("transaction.platform is invalid:%s. Use JTA or JDBC", transactionPlatform);
            throw new RuntimeException(msg);
        }
    }
}
