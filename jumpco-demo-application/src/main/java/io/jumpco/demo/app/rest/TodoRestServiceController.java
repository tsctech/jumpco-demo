package io.jumpco.demo.app.rest;

import io.jumpco.demo.todo.model.TodoItem;
import io.jumpco.demo.todo.model.TodoItemService;
import io.jumpco.demo.todo.model.TodoStatus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RepositoryRestController
@RequestMapping(path = "/todo/v1")
@ExposesResourceFor(TodoItem.class)
@Slf4j
public class TodoRestServiceController implements ResourceProcessor<RepositoryLinksResource> {
    @Autowired
    @Qualifier("serviceMessageSource")
    protected MessageSource dataMessageSource;

    @Autowired
    private TodoItemResourceAssembler todoItemResourceAssembler;

    @Autowired
    private HateoasPageableHandlerMethodArgumentResolver resolver;

    private TodoItemService todoItemService;

    @Autowired
    public TodoRestServiceController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<PagedResources<TodoItemResource>> list(@RequestParam(name = "status", required = false) String[] status,
                                                                 @PageableDefault(size = 50) Pageable pageable,
                                                                 HttpRequest request) {
        TodoItemPagedResourcesAssembler assembler = new TodoItemPagedResourcesAssembler(resolver, request);
        Set<TodoStatus> statusOptions = new HashSet<TodoStatus>();
        if (status != null && status.length != 0) {
            for (String item : status) {
                statusOptions.add(TodoStatus.valueOf(item));
            }
        }
        log.debug("list:status={}:{}", statusOptions, pageable);
        Page<TodoItem> result = todoItemService.findByStatus(statusOptions, pageable);
        return ResponseEntity.ok(assembler.toResource(result, todoItemResourceAssembler));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<TodoItemResource> get(@PathVariable("id") Long id) {
        return ResponseEntity.ok(todoItemResourceAssembler.toResource(todoItemService.load(id)));
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TodoItemResource> create(@RequestBody TodoItem item) {
        log.debug("create:saving:{}", item);
        return ResponseEntity.ok(todoItemResourceAssembler.toResource(todoItemService.create(item)));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<TodoItemResource> update(@PathVariable("id") Long id, @RequestBody Resource<TodoItem> item) {
        log.debug("update:saving:{}:{}", id, item);
        return ResponseEntity.ok(todoItemResourceAssembler.toResource(todoItemService.update(id, item.getContent())));
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable("id") Long id) {
        log.debug("delete:{}", id);
        todoItemService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @RequestMapping(path = "/{id}/status/{newStatus}", method = RequestMethod.PUT)
    public ResponseEntity<TodoItemResource> setStatus(@PathVariable("id") Long id, @PathVariable("newStatus") String newStatus) {
        log.debug("setStatus:{}:{}", id, newStatus);
        TodoStatus status = TodoStatus.valueOf(newStatus);
        return ResponseEntity.ok(todoItemResourceAssembler.toResource(todoItemService.updateStatus(id, status)));
    }

    @Override
    public RepositoryLinksResource process(RepositoryLinksResource resource) {
        resource.add(linkTo(TodoRestServiceController.class).withRel("todo"));
        return resource;
    }
}
