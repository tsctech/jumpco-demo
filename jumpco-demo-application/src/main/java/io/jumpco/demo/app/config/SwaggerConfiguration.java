package io.jumpco.demo.app.config;

import org.springframework.context.annotation.Bean;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
public class SwaggerConfiguration {
    private ApiInfo apiInfo() {
        String termsOfServiceUrl = "/";
        String contactName = "demo@jumpco.io";
        String license = "JumpCo Proprietary";
        String licenseUrl = "/";
        Contact contact = new Contact("JumpCo", contactName, "https://bitbucket.org/tsctech/jumpco-demo");
        return new ApiInfo("JumpCo Demo", "JumpCo Demo API", "1.0", termsOfServiceUrl, contact, license, licenseUrl);
    }

    @Bean
    public Docket swaggerPlugin() {
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo());
    }
}
