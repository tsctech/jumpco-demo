package io.jumpco.demo.app.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Corneil du Plessis.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorMessage {
    public static class PropertyMessage {
        private String message;
        private String property;
        public PropertyMessage(String property, String message) {
            this.property = property;
            this.message = message;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
        public String getProperty() {
            return property;
        }
        public void setProperty(String property) {
            this.property = property;
        }
        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("{");
            sb.append("property='").append(property).append('\'');
            sb.append(", message='").append(message).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    private List<PropertyMessage> details;
    private String message;
    private String stackTrace;
    public ErrorMessage(String message) {
        this.message = message;
    }
    public ErrorMessage(String message, String stackTrace) {
        this.message = message;
        this.stackTrace = stackTrace;
    }
    public void addDetail(String property, String detail) {
        if (details == null) {
            details = new ArrayList<>();
        }
        details.add(new PropertyMessage(property, detail));
    }
    public List<PropertyMessage> getDetails() {
        return details;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ErrorMessage{");
        sb.append("message='").append(message).append('\'');
        sb.append(", details=").append(details);
        sb.append(", stackTrace='").append(stackTrace).append('\'');
        sb.append('}');
        return sb.toString();
    }
    public void setDetails(List<PropertyMessage> details) {
        this.details = details;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public String getStackTrace() {
        return stackTrace;
    }
    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
