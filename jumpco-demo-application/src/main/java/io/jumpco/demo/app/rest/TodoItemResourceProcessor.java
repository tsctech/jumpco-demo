package io.jumpco.demo.app.rest;

import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class TodoItemResourceProcessor implements ResourceProcessor<TodoItemResource> {
    @Override
    public TodoItemResource process(TodoItemResource todoItemResource) {
        // TODO add extra links to be used
        return todoItemResource;
    }
}
