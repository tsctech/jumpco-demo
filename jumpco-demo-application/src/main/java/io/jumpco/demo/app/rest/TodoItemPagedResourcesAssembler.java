package io.jumpco.demo.app.rest;

import io.jumpco.demo.todo.model.TodoItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TodoItemPagedResourcesAssembler extends PagedResourcesAssembler<TodoItem> {
    @Autowired
    public TodoItemPagedResourcesAssembler(HateoasPageableHandlerMethodArgumentResolver resolver, HttpRequest request) {
        super(resolver, UriComponentsBuilder.fromHttpRequest(request).build());
    }

    public TodoItemPagedResourcesAssembler(HateoasPageableHandlerMethodArgumentResolver resolver, UriComponents baseUri) {
        super(resolver, baseUri);
    }
}
