package io.jumpco.demo.app.config;

import org.h2.server.web.WebServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class WebContainerConfig implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.scan(AppConfig.class.getPackage().getName());
        WebServlet h2Servlet = new WebServlet();
        ServletRegistration.Dynamic registration = servletContext.addServlet("H2Console", h2Servlet);
        registration.addMapping("/admin/h2/*");
        registration = servletContext.addServlet("dispatcher", new DispatcherServlet(context));
        registration.setLoadOnStartup(1);
        registration.addMapping("/*");
        registration.setInitParameter("throwExceptionIfNoHandlerFound", "true");

    }
}
