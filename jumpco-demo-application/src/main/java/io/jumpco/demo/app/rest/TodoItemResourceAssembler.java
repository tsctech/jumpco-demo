package io.jumpco.demo.app.rest;

import io.jumpco.demo.todo.model.TodoItem;
import io.jumpco.demo.todo.model.TodoStatus;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Component
public class TodoItemResourceAssembler extends ResourceAssemblerSupport<TodoItem, TodoItemResource> {
    public TodoItemResourceAssembler() {
        super(TodoRestServiceController.class, TodoItemResource.class);
    }

    @Override
    public TodoItemResource toResource(TodoItem item) {
        TodoItemResource result = new TodoItemResource(item, linkTo(TodoRestServiceController.class).slash(item.getId()).withSelfRel());
        for (TodoStatus s : TodoStatus.values()) {
            if (!s.equals(item.getStatus())) {
                result.add(linkTo(methodOn(TodoRestServiceController.class).setStatus(item.getId(), s.name())).withRel(s.name()));
            }
        }
        return result;
    }
}
