package io.jumpco.demo.app.rest;

import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

@Component
public class TodoItemResourcesProcessor implements ResourceProcessor<Resources<TodoItemResource>> {
    @Override
    public Resources<TodoItemResource> process(Resources<TodoItemResource> resources) {
        // TODO add any extra links needed
        return resources;
    }
}
