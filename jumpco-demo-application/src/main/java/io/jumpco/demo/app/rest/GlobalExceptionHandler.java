package io.jumpco.demo.app.rest;

import io.jumpco.demo.app.utils.ErrorMessage;
import io.jumpco.demo.app.utils.ErrorMessageUtils;
import io.jumpco.demo.todo.model.exceptions.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

@ControllerAdvice
@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    private ErrorMessageUtils errorMessageUtils;

    private void logRequest(HttpServletRequest request) {
        log.debug("REQUEST:URI:{}", request.getRequestURI());
        log.debug("REQUEST:METHOD:{}", request.getMethod());
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String name = headerNames.nextElement();
            Enumeration<String> headerValues = request.getHeaders(name);
            List<String> headers = new ArrayList<>();
            while (headerValues.hasMoreElements()) {
                headers.add(headerValues.nextElement());
            }
            log.debug("REQUEST:HEADER:{}: {}", name, headers);
        }
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String name = parameterNames.nextElement();
            log.debug("REQUEST:PARAMETER:{}={}", name, Arrays.asList(request.getParameterValues(name)));
        }
    }
    private void logRequest(HttpHeaders headers, WebRequest request) {
        log.debug("REQUEST:{}", request);
        log.debug("REQUEST:HEADERS:{}", headers);
        log.debug("REQUEST:URI:{}", request.getContextPath());
        for(Map.Entry<String, List<String>> entry: headers.entrySet()) {
            log.debug("REQUEST:HEADER:{}:{}", entry.getKey(), entry.getValue());
        }
        for(Map.Entry<String, String[]> entry: request.getParameterMap().entrySet()) {
            log.debug("REQUEST:PARAMETER:{}:{}", entry.getKey(), Arrays.asList(entry.getValue()));
        }
    }
    @ExceptionHandler(org.hibernate.exception.ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> handleHibernateConstraintViolationException(org.hibernate.exception.ConstraintViolationException x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleHibernateConstraintViolationException:{}", x.toString(), x);
        ErrorMessage errorMessage = new ErrorMessage(x.getMessage());
        Throwable cause = x.getCause();
        while (cause != null) {
            errorMessage.addDetail(cause.getClass().getCanonicalName(), cause.toString());
            cause = cause.getCause();
        }
        return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMessage);
    }
    @ExceptionHandler(PersistenceException.class)
    public ResponseEntity<ErrorMessage> handlePersistenceException(PersistenceException x, HttpServletRequest request) {
        if(x.getCause() instanceof org.hibernate.exception.ConstraintViolationException) {
            return handleHibernateConstraintViolationException((org.hibernate.exception.ConstraintViolationException) x.getCause(), request);
        } else {
            logRequest(request);
            log.error("handlePersistenceException:{}", x.getMessage(), x);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageUtils.createExceptionMessage(x));
        }
    }
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> handleConstraintViolationException(ConstraintViolationException x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleConstraintViolationException:{}", x.getMessage(), x);
        ErrorMessage errorMessage = errorMessageUtils.createErrorMessage(x);
        log.debug("handleConstraintViolationException:message:{}", errorMessage);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errorMessage);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public ResponseEntity<ErrorMessage> handleDuplicateException(DuplicateKeyException x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleDuplicateException:{}", x.toString(), x);
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ErrorMessage(x.getMessage()));
    }
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorMessage> handleEntityNotFoundException(EntityNotFoundException x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleEntityNotFoundException:{}", x.toString(), x);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage(x.getMessage()));
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorMessage> handleRuntimeException(RuntimeException x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleRuntimeException:{}", x, x);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageUtils.createExceptionMessage(x));
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception x, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logRequest(headers, request);
        ResponseEntity respnse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageUtils.createExceptionMessage(x));
        return respnse;
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logRequest(headers, request);
        ResponseEntity resonse = ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ErrorMessage(ex.getMessage()));
        return resonse;
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorMessage> handleThrowable(Throwable x, HttpServletRequest request) {
        logRequest(request);
        log.error("handleThrowable:{}", x, x);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessageUtils.createExceptionMessage(x));
    }
}
