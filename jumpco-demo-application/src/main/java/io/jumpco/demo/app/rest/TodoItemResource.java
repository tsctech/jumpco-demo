package io.jumpco.demo.app.rest;

import io.jumpco.demo.todo.model.TodoItem;
import org.springframework.hateoas.Link;

/**
 * Created by Corneil on 2016/08/23.
 */
public class TodoItemResource extends org.springframework.hateoas.Resource<TodoItem> {
    public TodoItemResource(TodoItem content, Iterable<Link> links) {
        super(content, links);
    }

    public TodoItemResource(TodoItem content, Link... links) {
        super(content, links);
    }
}
