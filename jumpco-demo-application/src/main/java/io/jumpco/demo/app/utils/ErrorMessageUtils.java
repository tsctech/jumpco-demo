package io.jumpco.demo.app.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
@Slf4j
public class ErrorMessageUtils {
    @Autowired
    @Qualifier("messageAccessor")
    private MessageSourceAccessor messageSourceAccessor;

    public ErrorMessage createExceptionMessage(Throwable x) {
        ErrorMessage errorMessage = new ErrorMessage(x.getMessage());
        Throwable cause = x.getCause();
        while (cause != null) {
            errorMessage.addDetail(cause.getClass().getCanonicalName(), cause.toString());
            cause = cause.getCause();
        }
        StringWriter writer = new StringWriter();
        PrintWriter pw = new PrintWriter(writer);
        x.printStackTrace(pw);
        pw.close();
        errorMessage.setStackTrace(writer.toString());
        return errorMessage;
    }

    public ErrorMessage createErrorMessage(ConstraintViolationException x) {
        ErrorMessage errorMessage = new ErrorMessage("ConstraintViolationException");
        for (ConstraintViolation violation : x.getConstraintViolations()) {
            log.error("handleConstraintViolationException:{}", violation);
            errorMessage.addDetail(makeProperty(violation.getPropertyPath()), violation.getMessage());
        }
        log.debug("createErrorMessage:{}", errorMessage);
        return errorMessage;
    }

    private static String makeProperty(String propertyName, String prefix) {
        if (propertyName.startsWith(prefix)) {
            return propertyName.substring(prefix.length());
        }
        return propertyName;
    }

    public ErrorMessage createErrorMessage(Errors errors) {
        String prefix = errors.getObjectName() + ".";
        ErrorMessage errorMessage = new ErrorMessage("BindException");
        if (errors.hasGlobalErrors()) {
            for (ObjectError err : errors.getGlobalErrors()) {
                String message = messageSourceAccessor.getMessage(err);
                String property = makeProperty(err.getCode(), prefix);
                errorMessage.addDetail(property, message);
                log.debug("Error:{}-{}", property, message);
            }
        }
        if (errors.hasFieldErrors()) {
            for (FieldError err : errors.getFieldErrors()) {
                log.debug("Error:{}.{}-{}:{}", err.getObjectName(), err.getField(), err.getDefaultMessage(), err.toString());
                StringBuilder message = new StringBuilder();
                if (err.getObjectName() != null && !err.getObjectName().equals("object")) {
                    message.append(err.getObjectName());
                    message.append('.');
                }
                if (err.getField() != null) {
                    message.append(err.getField());
                }
                errorMessage.addDetail(makeProperty(message.toString(), prefix), messageSourceAccessor.getMessage(err));
            }
        }
        log.debug("createErrorMessage:{}", errorMessage);
        return errorMessage;
    }

    public String makeProperty(Path path) {
        List<Path.Node> entries = new ArrayList<>();
        Iterator<Path.Node> iter = path.iterator();
        boolean hasArg = false;
        while (iter.hasNext()) {
            Path.Node node = iter.next();
            if (node.getName().startsWith("arg")) {
                hasArg = true;
            } else if (hasArg) {
                entries.add(node);
            }
        }
        if (entries.isEmpty()) {
            return path.toString();
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < entries.size(); i++) {
            if (i != 0) {
                builder.append('.');
            }
            builder.append(entries.get(i).toString());
        }
        return builder.toString();
    }
}
