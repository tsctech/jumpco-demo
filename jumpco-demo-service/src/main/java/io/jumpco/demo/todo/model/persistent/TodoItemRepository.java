package io.jumpco.demo.todo.model.persistent;

import io.jumpco.demo.todo.model.TodoStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collection;

@RepositoryRestResource(exported = false, itemResourceRel = "todo", collectionResourceRel = "todos")
public interface TodoItemRepository extends PagingAndSortingRepository<TodoItemEntity, Long> {
    Page<TodoItemEntity> findByStatusIn(Collection<TodoStatus> status, Pageable pageable);
}
