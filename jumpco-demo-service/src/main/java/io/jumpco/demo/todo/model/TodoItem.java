package io.jumpco.demo.todo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@EqualsAndHashCode(of = "description")
public class TodoItem {
    private Long id;
    @NotNull
    private String description;

    @NotNull
    private String details;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date createTime;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date updateTime;

    private TodoStatus status;
}
