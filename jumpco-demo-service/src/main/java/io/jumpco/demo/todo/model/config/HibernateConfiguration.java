package io.jumpco.demo.todo.model.config;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@Slf4j
public class HibernateConfiguration {
    @Autowired
    private Environment environment;

    @Value("${database.type}")
    private String databaseType;

    @Value("${transaction.platform:JDBC}")
    private String transactionPlatform;

    @Bean(name = "entityManagerFactory")
    @Autowired
    @Qualifier("demoDataSource")
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean lcemfb = new LocalContainerEntityManagerFactoryBean();
        if (transactionPlatform.equalsIgnoreCase("JTA")) {
            lcemfb.setJtaDataSource(dataSource);
        } else {
            lcemfb.setDataSource(dataSource);
        }
        lcemfb.setJpaDialect(new HibernateJpaDialect());
        lcemfb.setJpaVendorAdapter(jpaVendorAdapter());
        lcemfb.setPackagesToScan("io.jumpco.demo.todo.model.persistent");
        lcemfb.setPersistenceUnitName("demodb");
        lcemfb.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        lcemfb.setJpaProperties(hibernateProperties());
        lcemfb.afterPropertiesSet();
        return lcemfb.getObject();
    }

    Properties hibernateProperties() {
        Properties properties = new Properties();
        String dialect = environment.getProperty("hibernate.dialect");
        if(StringUtils.hasText(dialect)) {
            properties.setProperty("hibernate.dialect", dialect);
        } else {
            log.info("Using default dialect for {}", databaseType);
        }
        properties.setProperty("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto", "create"));
        properties.setProperty("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        properties.setProperty("hibernate.hbm2ddl.format", environment.getProperty("hibernate.hbm2ddl.format", "false"));
        if (transactionPlatform.equalsIgnoreCase("JTA")) {
            properties.setProperty("hibernate.transaction.factory_class", "org.hibernate.engine.transaction.internal.jta.CMTTransactionFactory");
            properties.setProperty("hibernate.transaction.jta.platform",
                environment.getProperty("hibernate.transaction.jta.platform", "WebSphereExtended"));
        } else if (transactionPlatform.equalsIgnoreCase("JDBC")) {
            properties.setProperty("hibernate.transaction.factory_class", "jdbc");
        } else {
            log.error("transaction.platform is invalid:{}. Default will be JDBC");
            properties.setProperty("hibernate.transaction.factory_class", "jdbc");
        }
        properties.setProperty("hibernate.hbm2ddl.import_files", environment.getProperty("hibernate.hbm2ddl.import_files", "import.sql"));
        log.debug("hibernateProperties:{}", properties);
        return properties;
    }

    @Bean(name = "jpaVendorAdapter")
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(Boolean.parseBoolean(environment.getProperty("database.showSql", "false")));
        if (StringUtils.isEmpty(databaseType)) {
            databaseType = environment.getProperty("database.type", "DB2");
        }
        boolean found = false;
        for (Database db : Database.values()) {
            if (db.name().equalsIgnoreCase(databaseType)) {
                jpaVendorAdapter.setDatabase(db);
                found = true;
                databaseType = db.name();
                break;
            }
        }
        String databaseDdl = environment.getProperty("hibernate.hbm2ddl.auto", "update");
        boolean generateDdl = databaseDdl.trim().equals("create") || databaseDdl.trim().equals("update");
        jpaVendorAdapter.setGenerateDdl(generateDdl);
        log.debug("databaseType:{}", databaseType);
        log.debug("generateDdl:{}", generateDdl);
        return jpaVendorAdapter;
    }
}
