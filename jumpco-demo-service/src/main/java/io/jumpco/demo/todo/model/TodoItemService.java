package io.jumpco.demo.todo.model;

import io.jumpco.demo.todo.model.exceptions.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Collection;

public interface TodoItemService {
    TodoItem create(TodoItem item);
    /**
     * Find a TodoItem by id.
     *
     * @param id of the entity
     * @return null if not found
     */
    TodoItem find(Long id);
    /**
     * Return page of TodoItems for all statuses.
     *
     * @param statuses if empty or null all TodoItems will be included, otherwise only TodoItems that match the criteria
     * @param pageable Includes page number, size and sorting criteria
     * @return
     */
    Page<TodoItem> findByStatus(Collection<TodoStatus> statuses, Pageable pageable);
    /**
     * Load a TodoItem by id.
     *
     * @param id of the entity
     * @return the TodoItem
     * @throws EntityNotFoundException if the TodoItem does not exist
     */
    TodoItem load(Long id);
    /**
     * Updates the TodoItem by loading the current item and modifying the properties that may be modified in update which excludes the status
     *
     * @param id   of the object
     * @param item TodoItem with values
     * @return the modified TodoItem
     */
    TodoItem update(Long id, TodoItem item);
    /**
     * Update the status of the TodoItem
     *
     * @param id
     * @param newStatus
     * @return
     */
    TodoItem updateStatus(Long id, TodoStatus newStatus);
    /**
     * Deletes the TodoItem
     *
     * @param id
     */
    void delete(Long id);
}
