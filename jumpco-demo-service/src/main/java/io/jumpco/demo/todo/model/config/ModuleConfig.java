package io.jumpco.demo.todo.model.config;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableJpaRepositories(basePackages = "io.jumpco.demo.todo.model.persistent")
@ComponentScan(basePackages = {"io.jumpco.demo.todo.model.config", "io.jumpco.demo.todo.model.persistent", "io.jumpco.demo.todo.model"})
@Import({DataSourceConfiguration.class, HibernateConfiguration.class})
public class ModuleConfig {
    @Bean
    public Mapper dozerBeanMapper() {
        DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
        List<Resource> mappingFiles = new ArrayList<Resource>();
        mappingFiles.add(new ClassPathResource("/*mappings.xml"));
        return dozerBeanMapper;
    }
}
