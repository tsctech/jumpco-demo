package io.jumpco.demo.todo.model;

public enum TodoStatus {
    TODO,
    BUSY,
    DONE
}
