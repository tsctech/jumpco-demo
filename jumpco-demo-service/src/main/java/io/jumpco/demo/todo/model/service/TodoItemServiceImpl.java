package io.jumpco.demo.todo.model.service;

import io.jumpco.demo.todo.model.TodoItem;
import io.jumpco.demo.todo.model.TodoItemService;
import io.jumpco.demo.todo.model.TodoStatus;
import io.jumpco.demo.todo.model.exceptions.EntityNotFoundException;
import io.jumpco.demo.todo.model.persistent.TodoItemEntity;
import io.jumpco.demo.todo.model.persistent.TodoItemRepository;
import lombok.extern.slf4j.XSlf4j;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service("todoItemService")
@XSlf4j(topic = "todo-todo")
@Transactional(rollbackFor = {Throwable.class})
public class TodoItemServiceImpl implements TodoItemService {
    @Autowired
    private TodoItemRepository todoItemRepository;

    @Autowired
    private Mapper mapper;

    @Override
    @Transactional
    public TodoItem create(TodoItem item) {
        try {
            log.entry(item);
            Assert.notNull(item, "item required");
            Assert.isNull(item.getId(), "item.id must be null");
            TodoItemEntity entity = mapper.map(item, TodoItemEntity.class);
            entity.setStatus(TodoStatus.TODO);
            Date now = new Date();
            entity.setCreateTime(now);
            entity.setUpdateTime(now);
            entity = todoItemRepository.save(entity);
            Assert.notNull(entity.getId(), "id not assigned");
            TodoItem result = mapper.map(entity, TodoItem.class);
            log.debug("create:{}={}", result, item);
            return result;
        } finally {
            log.exit();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public TodoItem find(Long id) {
        try {
            log.entry(id);
            Assert.notNull(id);
            log.debug("find:{}", id);
            TodoItemEntity entity = todoItemRepository.findOne(id);
            return mapper.map(entity, TodoItem.class);
        } finally {
            log.exit();
        }
    }

    private Page<TodoItem> map(Page<TodoItemEntity> page) {
        List<TodoItem> items = new ArrayList<TodoItem>();
        for (TodoItemEntity entity : page.getContent()) {
            items.add(mapper.map(entity, TodoItem.class));
        }
        return new PageImpl<TodoItem>(items, new PageRequest(page.getNumber(), page.getSize()), page.getTotalElements());
    }

    /**
     * The page cannot be used to retrieve next or previous directly. Call to this method need to be made with new Pageable.
     * @param statuses if empty or null all TodoItems will be included, otherwise only TodoItems that match the criteria
     * @param pageable Includes page number, size and sorting criteria
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TodoItem> findByStatus(Collection<TodoStatus> statuses, Pageable pageable) {
        try {
            log.entry(statuses, pageable);
            if (pageable == null) {
                pageable = new PageRequest(0, 50);
            }
            if (statuses == null || statuses.isEmpty()) {
                return map(todoItemRepository.findAll(pageable));
            }
            return map(todoItemRepository.findByStatusIn(statuses, pageable));
        } finally {
            log.exit();
        }
    }

    @Override
    @Transactional(readOnly = true, noRollbackFor = {EntityNotFoundException.class})
    public TodoItem load(Long id) {
        try {
            log.entry(id);
            Assert.notNull(id);
            TodoItemEntity entity = todoItemRepository.findOne(id);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("TodoItem:%d not found", id));
            }
            return mapper.map(entity, TodoItem.class);
        } finally {
            log.exit();
        }
    }

    @Override
    @Transactional
    public TodoItem update(Long id, TodoItem item) {
        try {
            log.entry(id, item);
            Assert.notNull(item, "item required");
            Assert.notNull(id, "id required");
            TodoItemEntity entity = todoItemRepository.findOne(id);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("TodoItem:%d not found", id));
            }
            Date now = new Date();
            entity.setDescription(item.getDescription());
            entity.setDetails(item.getDetails());
            entity.setStatus(item.getStatus());
            entity.setUpdateTime(now);
            entity = todoItemRepository.save(entity);
            TodoItem result = new TodoItem();
            mapper.map(entity, result);
            log.debug("update:{}={}", result, item);
            return result;
        } finally {
            log.exit();
        }
    }

    @Override
    @Transactional
    public TodoItem updateStatus(Long id, TodoStatus newStatus) {
        try {
            log.entry(id, newStatus);
            Assert.notNull(id, "id required");
            Assert.notNull(newStatus, "status required");
            TodoItemEntity entity = todoItemRepository.findOne(id);
            if (entity == null) {
                throw new EntityNotFoundException(String.format("TodoItem:%d not found", id));
            }
            log.debug("updateStatus:{}:{}->{}", id, entity.getStatus(), newStatus);
            entity.setStatus(newStatus);
            entity = todoItemRepository.save(entity);
            return mapper.map(entity, TodoItem.class);
        } finally {
            log.exit();
        }
    }



    @Override
    @Transactional
    public void delete(Long id) {
        todoItemRepository.delete(id);
    }
}
