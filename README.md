# JumpCo Demo WebSphere Liberty, Spring Rest, Spring Data, AngularJS

## Introduction
This projects demonstrates an AngularJS/Bootstrap UI consuming a Rest service using Spring Data JPA and Spring Data deployed with WebSphere Liberty Profile.

## Details
The following tools / frameworks and tools are in use:

* Spring IO Platform 2.0.7
    * Spring MVC Rest
    * Spring Data JPA
    * Spring Data Rest
    * Spring Hateos
    * Spring Data Rest HAL Browser
* SpringFox 2.4.0
* H2 Database 1.4.19
* WebSphere Liberty Profile 8.5.5.7
* WebSphere Liberty Profile Maven Plugin 1.2
* Front End
    * NPM
    * Bower
    * Grunt
    * Yeoman with Angular/fullstack generator
    * AngularJS 1.5.3
    * Bootstrap 3.3.x 
     
## Project Structure

* demo-project
    * jumpco-demo-api (API Spec and Documentation)
    * jumpco-demo-application (Web Application) 
    * jumpco-demo-service (Domain Service)
    * jumpco-demo-wlpcfg (WebSphere Liberty Profile Configuration)
    * jumpco-demo-web (Angular UI Web Project)
    
## Fetch the code
Click on [Clone](https://bitbucket.org/tsctech/jumpco-demo#clone) and follow instructions.

## Build
`mvnw install`

## Launch Rest Service

```
mvnw -pl jumpco-demo-service,jumpco-demo-application install -DskipTests=true
mvnw -DskipTests=true -pl jumpco-demo-wlpcfg package liberty:run-server
```

## Launch UI

After the initial build all the artifacts should have been downloaded and built.

```
cd jumpco-demo-web
grunt serve
```

## Build API Docs

You may need to install [Graphviz](jumpco-demo-api/build/asciidoc/html5). 

```
cd jumpco-demo-api
./gradlew asciidoctor
```

View the files under `jumpco-demo-api/build/asciidoc/html5` or `jumpco-demo-api/build/asciidoc/pdf`

