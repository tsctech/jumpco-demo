(function () {
	'use strict';
	angular.module('jumpCoDemoApp')
		.config(function ($stateProvider) {
			$stateProvider
				.state('home.todo', {
					url: 'todo',
					templateUrl: 'modules/todo/todo.html',
					controller: 'TodoCtrl',
					parent: 'home',
					data: {pageTitle: 'Todo List'}
				});
		});

})();
