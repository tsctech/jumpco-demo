(function () {
	'use strict';

	function processResults(promise, $scope) {
		promise.then(function (todos) {
			$scope.page = todos.page.number;
			$scope.size = todos.page.size;
			$scope.totalPages = todos.page.totalPages;
			if(todos._embedded) {
				$scope.todos = todos._embedded.todoItems;
				console.log('todos:' + JSON.stringify($scope.todos));
			} else {
				$scope.todos = null;
			}
			$scope.todoLinks = todos._links;
		}, function (response) {
			// TODO handle error response
		});
	}

	angular.module('jumpCoDemoApp')
		.controller('TodoCtrl', ['$scope', 'TodoRestService', function ($scope, TodoRestService) {
			$scope.page = 0;
			$scope.totalPages = 0;
			$scope.size = 10;
			$scope.todos = null;
			$scope.todoLinks = null;

			var promise = TodoRestService.findByStatus();
			processResults(promise, $scope);
			return {
				first: function (size) {
					// TODO check if first not null
					var promise = TodoRestService.followLink($scope.todoLinks.first, undefined, size);
					processResults(promise, $scope);
				},
				last: function () {
					// TODO check if last not null
					var promise = TodoRestService.followLink($scope.todoLinks.last);
					processResults(promise, $scope);
				},
				next: function () {
					// TODO check if next not null
					var promise = TodoRestService.followLink($scope.todoLinks.next);
					processResults(promise, $scope);
				},
				prev: function () {
					// TODO check if prev not null
					var promise = TodoRestService.followLink($scope.todoLinks.prev);
					processResults(promise, $scope);
				},
				page: function (page, size) {
					var promise = TodoRestService.findByStatus(page, size);
					processResults(promise, $scope);
				}
			}
		}]);
})();
