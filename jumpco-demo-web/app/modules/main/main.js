'use strict';

angular.module('jumpCoDemoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'modules/main/main.html',
        controller: 'MainCtrl'
      });
  });
