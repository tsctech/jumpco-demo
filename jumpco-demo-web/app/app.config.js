(function () {
	'use strict';

	angular.module('jumpCoDemoApp').run(function ($location) {
		$location.path('/');
	}).config(function ($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/error');
		$stateProvider.state('error', {url: '/error', templateUrl: '404.html'});
	});
})();
