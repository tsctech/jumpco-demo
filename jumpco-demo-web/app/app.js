(function () {
	'use strict';

	angular.module('jumpCoDemoApp', [
		'config',
		'ui.router',
		'ui.bootstrap',
		'ngMessages',
		'ngResource'
	]);
})();
