(function () {
	'use strict';

	angular.module('jumpCoDemoApp').service('TodoRestService', ['$q', '$resource', '$log', 'todoCache', 'ENV',
		function ($q, $resource, $log, todoCache, ENV) {
			function getLink(link, page, size) {
				var deferred = $q.defer();
				var params = {};
				if (page != undefined) {
					params.page = page;
				}
				if (size != undefined) {
					params.size = size;
				}
				var TodoLinkResource = $resource(link, {}, {link:{method:'GET'}});
				TodoLinkResource.link(params).$promise.then(function (todos) {
					deferred.resolve(todos);
				}, function (response) {
					deferred.reject(response);
				});
				return deferred.promise;
			}
			return {
				findByStatus: function (page, size) {
					return getLink(ENV.rmmServiceUrl, page, size);
				},
				followLink: function(link, page, size) {
					return getLink(link, page, size);
				},
				create: function (todo) {
					var deferred = $q.defer();
					var params = {};
					if (page != undefined) {
						params.page = page;
					}
					if (size != undefined) {
						params.size = size;
					}
					var TodoResourceCreate = $resource(ENV.rmmServiceUrl, {}, {create: {method: 'POST'}});
					TodoResourceCreate.create(todo).$promise.then(function (result) {
						deferred.resolve(result);
					}, function (response) {
						deferred.reject(response);
					});
					return deferred.promise;
				}
			};
		}
	]);
})();
