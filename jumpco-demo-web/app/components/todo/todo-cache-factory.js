(function () {
	'use strict';

	angular.module('jumpCoDemoApp')
		.factory('todoCache', ['$cacheFactory', function ($cacheFactory) {
			return $cacheFactory('todo-cache');
		}]);
})();
