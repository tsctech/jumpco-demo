// Generated on 2015-04-30 using generator-angular 0.11.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Configurable paths for the application
	var appConfig = {
		app: require('./bower.json').appPath || 'app',
		dist: 'dist'
	};
	var target = grunt.option('target') || 'dev';

	var modRewrite = require('connect-modrewrite');

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		jumpco: appConfig,

		// Watches files for changes and runs tasks based on the changed files
		watch: {
			bower: {
				files: ['bower.json'],
				tasks: ['wiredep']
			},
			injectJS: {
				files: [
					'<%= jumpco.app %>/{modules,components,services,directives,filters}/**/*.js',
					'!<%= jumpco.app %>/{modules,components,services,directives,filters}/**/*.spec.js',
					'!<%= jumpco.app %>/{modules,components,services,directives,filters}/**/*.mock.js',
					'!<%= jumpco.app %>/app.js'],
				tasks: ['injector:scripts']
			},
			jsTest: {
				files: [
					//'test/spec/{,*/}*.js',
					'<%= jumpco.app %>/{modules,components,services,directives,filters}/**/*.spec.js',
				],
				tasks: ['newer:jshint:test', 'karma']
			},
			styles: {
				files: ['<%= jumpco.app %>/assets/css/*.css'],
				tasks: ['newer:copy:styles', 'autoprefixer']
			},
			injectLess: {
				files: [
					'<%= jumpco.app %>/app.less',
					'<%= jumpco.app %>/{modules,components,assets,directives}/**/*.less'
				],
				tasks: ['injector:less']
			},
			less: {
				files: [
					'<%= jumpco.app %>/app.less',
					'<%= jumpco.app %>/{modules,components,assets,directives}/**/*.less'
				],
				tasks: ['less:server', 'autoprefixer']
			},
			gruntfile: {
				files: ['Gruntfile.js']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				//tasks: ['less'],
				files: [
					'<%= jumpco.app %>/{,*/}*.html',
					'<%= jumpco.app %>/{,*/}*.js',
					'<%= jumpco.app %>/{,*/}*.less',
					'{.tmp,<%= jumpco.app %>}/{,*/}*.css',

					'{.tmp,<%= jumpco.app %>}/{modules,components,directives}/**/*.html',

					'<%= jumpco.app %>/{modules,components,directives}/**/*.html',
					'<%= jumpco.app %>/{modules,components,directives}/**/*.js',
					'<%= jumpco.app %>/{modules,components,assets,directives}/**/*.less',

					'!{.tmp,<%= jumpco.app %>}/{modules,components,directives}/**/*.spec.js',
					'!{.tmp,<%= jumpco.app %>}/{modules,components,directives}/**/*.mock.js',

					'<%= jumpco.app %>/assets/images/{,**/}*.{png,jpg,jpeg,gif,webp,svg}'
				]
			}
		},

		ngconstant: {
			options: {
				name: 'config',
				dest: 'app/config.js',
				space: '  '
			},
			development: {
				constants: {
					ENV: {
						name: 'development',
						rmmServiceUrl: 'http://localhost\\:9080/jumpco-demo/todo/v1'
					}
				}
			},
			production: {
				constants: {
					ENV: {
						name: 'production',
						rmmServiceUrl: 'http://localhost\\:9080/jumpco-demo/todo/v1'
					}
				}
			}
		},
		// The actual grunt server settings
		connect: {
			options: {
				port: 9000,
				// Change this to '0.0.0.0' to access the server from outside.
				hostname: 'localhost',
				livereload: 37729
			},
			livereload: {
				options: {
					open: true,
					middleware: function (connect) {
						return [
							function (req, res, next) {
								res.setHeader('Access-Control-Allow-Origin', '*');
								res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
								res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
								res.setHeader('Access-Control-Request-Headers', 'x-requested-with, Content-Type, origin, authorization, accept, client-security-token');
								return next();
							},
							modRewrite(['^[^\\.]*$ /index.html [L]']),
							connect.static('.tmp'),
							connect().use(
								'/bower_components',
								connect.static('./bower_components')
							),
							/*connect().use(
							 '/app/styles',
							 connect.static('./app/styles')
							 ),*/
							connect().use(
								'/app/assets',
								connect.static('./app/assets')
							),
							connect.static(appConfig.app)
						];
					}
				}
			},
			test: {
				options: {
					port: 9011,
					middleware: function (connect) {
						return [
							connect.static('.tmp'),
							connect.static('test'),
							connect().use(
								'/bower_components',
								connect.static('./bower_components')
							),
							connect.static(appConfig.app)
						];
					}
				}
			},
			dist: {
				options: {
					open: true,
					base: '<%= jumpco.dist %>'
				}
			}
		},

		// Compiles Less to CSS
		less: {
			options: {
				paths: [
					'<%= jumpco.app %>/bower_components',
					'<%= jumpco.app %>',
					'<%= jumpco.app %>/{modules,components,directives}'
				]
			},

			server: {
				files: {
					'.tmp/app.css': '<%= jumpco.app %>/app.less'
				}
			},
		},

		injector: {
			// Inject application script files into index.html (doesn't include bower)
			scripts: {
				options: {
					transform: function (filePath) {
						filePath = filePath.replace('/app/', '');
						filePath = filePath.replace('/.tmp/', '');
						return '<script src="' + filePath + '"></script>';
					},
					starttag: '<!-- injector:js -->',
					endtag: '<!-- endinjector -->'
				},
				files: {
					'<%= jumpco.app %>/index.html': [
						[
							'{.tmp,<%= jumpco.app %>}/{modules,components,services,directives,filters}/**/*.js',

							'!{.tmp,<%= jumpco.app %>}/app.js',
							'!{.tmp,<%= jumpco.app %>}/{modules,components,services,directives,filters}/**/*.spec.js',
							'!{.tmp,<%= jumpco.app %>}/{modules,components,services,directives,filters}/**/*.mock.js'
						]
					]
				}
			},

			// Inject component less into app.less
			less: {
				options: {
					transform: function (filePath) {
						filePath = filePath.replace('/app/', '');
						filePath = filePath.replace('/modules/', '');
						filePath = filePath.replace('/components/', '');
						filePath = filePath.replace('/directives/', '');
						return '@import \'' + filePath + '\';';
					},
					starttag: '// injector',
					endtag: '// endinjector'
				},
				files: {
					'<%= jumpco.app %>/app.less': [
						'<%= jumpco.app %>/{modules,components,directives}/**/*.less',
						'!<%= jumpco.app %>/app.less',
					]
				}
			}
		},

		// Make sure code styles are up to par and there are no obvious mistakes
		jshint: {
			options: {
				jshintrc: '.jshintrc',
				reporter: require('jshint-stylish'),
				force: true
			},
			all: {
				src: [
					'Gruntfile.js',
					'<%= jumpco.app %>/scripts/{,*/}*.js'
				]
			},
			test: {
				options: {
					jshintrc: 'test/.jshintrc'
				},
				src: ['test/spec/{,*/}*.js']
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= jumpco.dist %>/{,*/}*',
						'!<%= jumpco.dist %>/.git{,*/}*'
					]
				}]
			},
			server: '.tmp'
		},

		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 1 version']
			},
			server: {
				options: {
					map: true
				},
				files: [{
					expand: true,
					//cwd: '.tmp/styles/',
					//src: '{,*/}*.css',
					//dest: '.tmp/styles/'

					cwd: '.tmp/',
					src: '{,*/}*.css',
					dest: '.tmp/'
				}]
			},
			dist: {
				files: [{
					expand: true,
					//cwd: '.tmp/styles/',
					//src: '{,*/}*.css',
					//dest: '.tmp/styles/'

					cwd: '.tmp/',
					src: '{,*/}*.css',
					dest: '.tmp/'
				}]
			}
		},

		// Automatically inject Bower components into the app
		wiredep: {
			app: {
				src: ['<%= jumpco.app %>/index.html'],
				ignorePath: /\.\.\//,
				exclude: [/ui-bootstrap-tpls.js/, /bootstrap.css/, /font-awesome.css/, /bootstrap-select.js/, /animate.css/]
			},
			test: {
				devDependencies: true,
				src: '<%= karma.unit.configFile %>',
				ignorePath: /\.\.\//,
				fileTypes: {
					js: {
						block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
						detect: {
							js: /'(.*\.js)'/gi
						},
						replace: {
							js: '\'{{filePath}}\','
						}
					}
				},
				exclude: [/ui-bootstrap-tpls.js/, /bootstrap.css/, /font-awesome.css/, /bootstrap-select.js/, /animate.css/]
			}
		},

		// Renames files for browser caching purposes
		filerev: {
			dist: {
				src: [
					'<%= jumpco.dist %>/scripts/{,*/}*.js',
					'<%= jumpco.dist %>/styles/{,*/}*.css',
					'<%= jumpco.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
					'<%= jumpco.dist %>/styles/fonts/*',

					'<%= jumpco.dist %>/assets/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
					'<%= jumpco.dist %>/assets/fonts/*'
				]
			}
		},

		// Reads HTML for usemin blocks to enable smart builds that automatically
		// concat, minify and revision files. Creates configurations in memory so
		// additional tasks can operate on them
		useminPrepare: {
			html: ['<%= jumpco.app %>/index.html'],
			options: {
				dest: '<%= jumpco.dist %>',
				flow: {
					html: {
						steps: {
							js: ['concat', 'uglifyjs'],
							css: ['cssmin']
						},
						post: {}
					}
				}
			}
		},

		// Performs rewrites based on filerev and the useminPrepare configuration
		usemin: {
			html: ['<%= jumpco.dist %>/{,*/}*.html'],
			css: ['<%= jumpco.dist %>/styles/app.css'],
			options: {
				assetsDirs: [
					'<%= jumpco.dist %>',
					'<%= jumpco.dist %>/images',
					'<%= jumpco.dist %>/styles'
				]
			}
		},

		// The following *-min tasks will produce minified files in the dist folder
		// By default, your `index.html`'s <!-- Usemin block --> will take care of
		// minification. These next options are pre-configured if you do not wish
		// to use the Usemin blocks.
		cssmin: {
			dist: {
				files: {
					/*'<%= jumpco.dist %>/styles/main.css': [
					 '.tmp/styles/{,*!/}*.css'
					 ]*/
					'<%= jumpco.dist %>/styles/vendor.css': [
						'.tmp/vendor.css'
					],
					'<%= jumpco.dist %>/styles/app.css': [
						'.tmp/app.css'
					]
				}
			}
		},
		uglify: {
			dev: {
				options: {
					mangle: false,
					beautify: true
				},
				files: {
					'<%= jumpco.dist %>/interfront-ctp-gui-min.js': [
						'<%= jumpco.app %>/scripts/{,*/}*.js'
					]
				}
			},
			prod: {
				options: {
					mangle: true,
					beautify: false
				},
				files: {
					'<%= jumpco.dist %>/interfront-ctp-gui-min.js': [
						'<%= jumpco.app %>/scripts/{,*/}*.js'
					]
				}
			}
		},
		concat: {
			dist: {}
		},

		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= jumpco.app %>/images',
					src: '{,*/}*.{ico,png,jpg,jpeg,gif}',
					dest: '<%= jumpco.dist %>/images'
				}]
			}
		},

		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= jumpco.app %>/images',
					src: '{,*/}*.svg',
					dest: '<%= jumpco.dist %>/images'
				}]
			}
		},

		htmlmin: {
			dist: {
				options: {
					collapseWhitespace: true,
					conservativeCollapse: true,
					collapseBooleanAttributes: true,
					removeCommentsFromCDATA: true,
					removeOptionalTags: true
				},
				files: [{
					expand: true,
					cwd: '<%= jumpco.dist %>',
					src: ['*.html', 'views/{,**/}*.html', 'modules/{,**/}*.html'],
					dest: '<%= jumpco.dist %>'
				}]
			}
		},

		// ng-annotate tries to make the code safe for minification automatically
		// by using the Angular long form for dependency injection.
		ngAnnotate: {
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/concat/scripts',
					src: '*.js',
					dest: '.tmp/concat/scripts'
				}]
			}
		},

		// Replace Google CDN references
		cdnify: {
			dist: {
				html: ['<%= jumpco.dist %>/*.html']
			}
		},

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= jumpco.app %>',
					dest: '<%= jumpco.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'*.html',
						'views/{,**/}*.html',
						'data/{,**/}*.json',

						'assets/images/**/*',
						'assets/fonts/**/*',
						'modules/**/*',
						'services/**/*',
						'components/**/*',
						//'libs/**/*',
						//'template/**/*',

						'!services/{,**/}*.spec.js',
						'!modules/{,**/}*.spec.js',
						'!components/{,**/}*.spec.js'
					]
				}, {
					expand: true,
					cwd: '.tmp/images',
					dest: '<%= jumpco.dist %>/images',
					src: ['generated/*']
				}, {
					expand: true,
					cwd: 'bower_components/font-awesome',
					src: 'fonts/*',
					dest: '<%= jumpco.dist %>'
				}, {
					expand: true,
					cwd: 'bower_components/bootstrap/dist',
					src: 'fonts/*',
					dest: '<%= jumpco.dist %>'
				}, {
					expand: true,
					cwd: '<%= jumpco.app %>/assets/images',
					src: 'tree-control/*',
					dest: '<%= jumpco.dist %>/images'
				}],
			},
			styles: {
				expand: true,
				cwd: '<%= jumpco.app %>',
				dest: '.tmp/',
				src: ['app.css', 'assets/css/**', 'libs/**/*.css']
			}
		},

		// Run some tasks in parallel to speed up the build process
		concurrent: {
			server: [
				'less',
				'copy:styles',
			],
			test: [
				'less',
				'copy:styles',
			],
			dist: [
				'less',
				'copy:styles',
				'imagemin',
				'svgmin'
			]
		},

		// Test settings
		karma: {
			unit: {
				configFile: 'karma.conf.js',
				singleRun: true
			}
		}
	});


	grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			'clean:server',
			'ngconstant:development',
			'injector:less',
			'injector',
			'wiredep',
			'concurrent:server',
			'autoprefixer:server',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('test', [
		'clean:server',
		'ngconstant:production',
		'injector:less',
		'injector',
		'wiredep',
		'concurrent:test',
		'autoprefixer',
		'connect:test',
		'karma'
	]);

	grunt.registerTask('build', [
		'clean:dist',
		'injector:less',
		'injector',
		'wiredep',
		'useminPrepare',
		'concurrent:dist',
		'autoprefixer',
		'concat',
		'ngAnnotate',
		'copy:dist',
		'cdnify',
		'cssmin',
		//'uglify:' + target,
		'uglify:generated',
		//'filerev',
		'usemin',
		'htmlmin'
	]);

	grunt.registerTask('default', [
		'newer:jshint',
		'test',
		'build'
	]);
};
